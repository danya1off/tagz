//
//  TagsTVCell.swift
//  Tagz
//
//  Created by Jeyhun Danyalov on 5/11/18.
//  Copyright © 2018 Jeyhun Danyalov. All rights reserved.
//

import UIKit

class TagsTVCell: UITableViewCell {
    var tagCategory: TagCategory? {
        didSet {
            guard let tagCat = tagCategory else {return}
            categoryTitle.text = tagCat.title
            tags.text = tagCat.tags
            countLabel.text = "\(tagCat.tagsCount) tags"
        }
    }
    
    private func tagsCount(tagString: String) -> Int {
        let count = tagString.split(separator: " ").count
        return count
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    private let categoryTitle: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor.foregroundColor
        label.font = Constants.tagTitleFont
        return label
    }()
    
    private let countLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = Constants.tagsFont
        label.textColor = UIColor.foregroundColor
        return label
    }()
    
    private let line: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.foregroundColor
        return view
    }()
    
    private let tags: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .lightGray
        label.numberOfLines = 0
        label.font = Constants.tagsFont
        return label
    }()
    
    let mainCellView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        view.layer.cornerRadius = 5
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowOpacity = 0.2
        view.layer.shadowRadius = 5
        return view
    }()
    
    fileprivate func setup() {
        
        backgroundColor = .white
        
        
        let stack = UIStackView(arrangedSubviews: [categoryTitle, countLabel])
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.axis = .horizontal
        stack.alignment = .fill
        stack.distribution = .fill
        
        addSubview(mainCellView)
        mainCellView.addSubview(stack)
        mainCellView.addSubview(line)
        mainCellView.addSubview(tags)
        
        NSLayoutConstraint.activate([
            
            mainCellView.topAnchor.constraint(equalTo: topAnchor, constant: 8),
            mainCellView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            mainCellView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            mainCellView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -8),
            
            stack.leadingAnchor.constraint(equalTo: mainCellView.leadingAnchor, constant: 15),
            stack.trailingAnchor.constraint(equalTo: mainCellView.trailingAnchor, constant: -15),
            stack.topAnchor.constraint(equalTo: mainCellView.topAnchor, constant: 15),
            
            line.leadingAnchor.constraint(equalTo: categoryTitle.leadingAnchor),
            line.trailingAnchor.constraint(equalTo: countLabel.trailingAnchor),
            line.heightAnchor.constraint(equalToConstant: 1),
            line.topAnchor.constraint(equalTo: stack.bottomAnchor, constant: 5),
            
            tags.leadingAnchor.constraint(equalTo: line.leadingAnchor),
            tags.trailingAnchor.constraint(equalTo: line.trailingAnchor),
            tags.topAnchor.constraint(equalTo: line.bottomAnchor, constant: 15),
            tags.bottomAnchor.constraint(equalTo: mainCellView.bottomAnchor, constant: -15),
            
            ])
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("System error!")
    }
}
