//
//  TagCollectionCell.swift
//  Tagz
//
//  Created by Jeyhun Danyalov on 5/9/18.
//  Copyright © 2018 Jeyhun Danyalov. All rights reserved.
//

import UIKit

class TagsCVCell: UICollectionViewCell {
    
    var tagCategory: TagCategory? {
        didSet {
            guard let tagCat = tagCategory else {return}
            categoryTitle.text = tagCat.title
            tags.text = tagCat.tags
            countLabel.text = "\(tagCat.tagsCount) tags"
        }
    }
    
    private func tagsCount(tagString: String) -> Int {
        let count = tagString.split(separator: " ").count
        return count
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    private let categoryTitle: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor.foregroundColor
        label.font = Constants.tagTitleFont
        return label
    }()
    
    private let countLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = Constants.tagsFont
        label.textColor = UIColor.foregroundColor
        return label
    }()
    
    private let line: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.foregroundColor
        return view
    }()
    
    private let tags: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .lightGray
        label.numberOfLines = 0
        label.font = Constants.tagsFont
        return label
    }()
    
    fileprivate func setup() {
        
        backgroundColor = .white
        layer.cornerRadius = 5
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize.zero
        layer.shadowOpacity = 0.2
        layer.shadowRadius = 5
        
        let stack = UIStackView(arrangedSubviews: [categoryTitle, countLabel])
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.axis = .horizontal
        stack.alignment = .fill
        stack.distribution = .fill
        
//        addSubview(categoryTitle)
//        addSubview(countLabel)
        addSubview(stack)
        addSubview(line)
        addSubview(tags)
        
        NSLayoutConstraint.activate([
            
            stack.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 15),
            stack.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -15),
            stack.topAnchor.constraint(equalTo: topAnchor, constant: 15),
            
            line.leadingAnchor.constraint(equalTo: categoryTitle.leadingAnchor),
            line.trailingAnchor.constraint(equalTo: countLabel.trailingAnchor),
            line.heightAnchor.constraint(equalToConstant: 1),
            line.topAnchor.constraint(equalTo: stack.bottomAnchor, constant: 5),
            
            tags.leadingAnchor.constraint(equalTo: line.leadingAnchor),
            tags.trailingAnchor.constraint(equalTo: line.trailingAnchor),
            tags.topAnchor.constraint(equalTo: line.bottomAnchor, constant: 15),
            tags.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -15),
            
            ])
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("System error!")
    }
    
}
