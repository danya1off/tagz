//
//  MainTagCell.swift
//  Tagz
//
//  Created by Jeyhun Danyalov on 5/7/18.
//  Copyright © 2018 Jeyhun Danyalov. All rights reserved.
//

import UIKit

class TagCategoryCell: UICollectionViewCell {
    
    var tagItem: TagCollection? {
        didSet {
            guard let tag = tagItem else {return}
            tagTitle.text = tag.title
            iconView.image = UIImage(named: tag.imageName)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    private let iconView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        imageView.tintColor = UIColor.foregroundColor
        return imageView
    }()
    
    private let tagTitle: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor.foregroundColor
        label.font = UIFont.systemFont(ofSize: 12, weight: .medium)
        label.textAlignment = .center
        return label
    }()
    
    fileprivate func setup() {
        backgroundColor = .white //UIColor.cellBgColor
        layer.cornerRadius = 5
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize.zero
        layer.shadowOpacity = 0.2
        layer.shadowRadius = 5
        
        addSubview(iconView)
        addSubview(tagTitle)
        
        NSLayoutConstraint.activate([
            
            iconView.centerXAnchor.constraint(equalTo: centerXAnchor),
            iconView.topAnchor.constraint(equalTo: topAnchor, constant: 20),
            iconView.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.4),
            
            tagTitle.widthAnchor.constraint(equalTo: widthAnchor),
            tagTitle.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -15)
            
            ])
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
