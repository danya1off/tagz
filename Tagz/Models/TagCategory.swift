//
//  TagCategory.swift
//  Tagz
//
//  Created by Jeyhun Danyalov on 5/9/18.
//  Copyright © 2018 Jeyhun Danyalov. All rights reserved.
//

import Foundation

struct TagCategory {
    
    var id = ""
    var title = ""
    var tags = "" {
        willSet {
            tagsCount = calculateTagsCount(tagString: newValue)
        }
    }
    var tagsCount = 0
    
    
    private func calculateTagsCount(tagString: String) -> Int {
        let count = tagString.split(separator: " ").count
        return count
    }
}
