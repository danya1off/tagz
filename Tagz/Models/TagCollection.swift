//
//  Tag.swift
//  Tagz
//
//  Created by Jeyhun Danyalov on 5/7/18.
//  Copyright © 2018 Jeyhun Danyalov. All rights reserved.
//

import Foundation

struct TagCollection {
    var id = ""
    var title = ""
    var imageName = ""
}
