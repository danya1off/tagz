//
//  TagModel+CoreDataProperties.swift
//  Tagz
//
//  Created by Jeyhun Danyalov on 5/11/18.
//  Copyright © 2018 Jeyhun Danyalov. All rights reserved.
//
//

import Foundation
import CoreData


extension TagModel {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<TagModel> {
        return NSFetchRequest<TagModel>(entityName: "TagModel")
    }

    @NSManaged public var title: String?
    @NSManaged public var tags: String?

    var getTag: TagCategory {
        var tagCategory = TagCategory()
        if let title = self.title {
            tagCategory.title = title
        }
        if let tags = self.tags {
            tagCategory.tags = tags
        }
        return tagCategory
    }
    
}
