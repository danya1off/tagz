//
//  Extension.swift
//  Tagz
//
//  Created by Jeyhun Danyalov on 5/6/18.
//  Copyright © 2018 Jeyhun Danyalov. All rights reserved.
//

import UIKit

// MARK: - UINavigationController Extension
extension UINavigationController {
    
    override open var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    func transparentNavigation(hidesOnSwipe: Bool = false) {
        
        guard let pacificoFont = UIFont(name: "Pacifico", size: 20) else {
            return
        }

        navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.customPink, NSAttributedStringKey.font: pacificoFont]
        
        navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationBar.shadowImage = UIImage()
        navigationBar.isTranslucent = true
        view.backgroundColor = .clear
        if hidesOnSwipe {
            hidesBarsOnSwipe = true
        }
    }
    
}

extension UIViewController {
    
    func errorAlert(title: String = "Error", message: String, completion: (() -> Void)? = nil) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let closeAction = UIAlertAction(title: "Close", style: .cancel) { action in
            if let comp = completion {
                comp()
            }
        }
        alertController.addAction(closeAction)
        present(alertController, animated: true)
    }
    
    func showToast(message : String) {
        
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.width/2 - 75, y: self.view.frame.size.height-200, width: 150, height: 35))
        toastLabel.backgroundColor = UIColor.bgColor.withAlphaComponent(0.8)
        toastLabel.textColor = UIColor.customPink
        toastLabel.textAlignment = .center;
        toastLabel.font = Constants.tagsFont
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 5;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
}

// MARK: - UIColor Extension
extension UIColor {
    
    static var bgColor: UIColor {
        return UIColor(red:0.20, green:0.21, blue:0.27, alpha:1.0)
    }
    
    static var customPink: UIColor {
//        return UIColor(red:0.95, green:0.92, blue:0.91, alpha:1.0)
        return UIColor(red:0.91, green:0.30, blue:0.47, alpha:1.0)
    }
    
    static var foregroundColor: UIColor {
        return UIColor(red:0.93, green:0.40, blue:0.24, alpha:1.0)
    }
    
    static var cellBgColor: UIColor {
        return UIColor(red:0.24, green:0.25, blue:0.32, alpha:1.0)
    }
    
    static var customRed: UIColor {
        return UIColor(red:0.91, green:0.30, blue:0.24, alpha:1.0)
    }
    
    static var customBlue: UIColor {
        return UIColor(red:0.20, green:0.60, blue:0.86, alpha:1.0)
    }
    
}

class CustomActivityIndicator: UIActivityIndicatorView {
    
    init(style: UIActivityIndicatorViewStyle, view: UIView) {
        super.init(activityIndicatorStyle: style)
        view.addSubview(self)
        self.hidesWhenStopped = true
        self.center = view.center
        self.color = UIColor.customPink
        
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
