//
//  TagsCategoriesVC.swift
//  Tagz
//
//  Created by Jeyhun Danyalov on 5/6/18.
//  Copyright © 2018 Jeyhun Danyalov. All rights reserved.
//

import UIKit
import Firebase

class TagsCategoriesVC: BaseCollectionVC {
    
    private var tagsCollections = [TagCollection]()
    
    override func viewDidLoad() {
        
        navigationTitle = "tagz."
        collectionViewCell = TagCategoryCell.self
        super.viewDidLoad()
        
        let settings = db.settings
        settings.areTimestampsInSnapshotsEnabled = true
        db.settings = settings

        populateData()
        
    }
    
    fileprivate func populateData() {
        activityIndicator.startAnimating()
        var popularTag: TagCollection!
        db.collection(Constants.tags).getDocuments { (snapshot, error) in
            if let err = error {
                print(err.localizedDescription)
            } else {
                for doc in snapshot!.documents {
                    var tag = TagCollection()
                    tag.id = doc.documentID
                    if let title = doc.data()["title"] as? String {
                        tag.title = title
                    }
                    if let image = doc.data()["image"] as? String {
                        tag.imageName = image
                    }
                    if tag.id == "popular" {
                        popularTag = tag
                        continue
                    }
                    self.tagsCollections.append(tag)
                }
            }
            self.tagsCollections.insert(popularTag, at: 0)
            DispatchQueue.main.async {
                self.activityIndicator.stopAnimating()
                self.collectionView.reloadData()
            }
        }
    }
    
    fileprivate func selectRow(tagCollection: TagCollection) {
        let tagsVC = TagsVC()
        tagsVC.tagCollection = tagCollection
        navigationController?.pushViewController(tagsVC, animated: true)
    }
    
}

extension TagsCategoriesVC {
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tagsCollections.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = (view.frame.width / 3) - 20
        return CGSize(width: size, height: size)
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: TagCategoryCell.self), for: indexPath) as! TagCategoryCell
        
        let tagCollection = tagsCollections[indexPath.item]
        cell.tagItem = tagCollection
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let selectedTag = tagsCollections[indexPath.item]
        selectRow(tagCollection: selectedTag)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
    }
    
}
