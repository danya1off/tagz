//
//  ViewController.swift
//  Tagz
//
//  Created by Jeyhun Danyalov on 5/6/18.
//  Copyright © 2018 Jeyhun Danyalov. All rights reserved.
//

import UIKit
import GoogleMobileAds

class MainTabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }

}

extension MainTabBarController {
    
    fileprivate func setup() {
        
        let tagsCategoriesVC = generateNavigationController(for: TagsCategoriesVC(), normal: #imageLiteral(resourceName: "category"), selected: #imageLiteral(resourceName: "category_selected"))

        let usersTagsVC = generateNavigationController(for: UsersTagsVC(), normal: #imageLiteral(resourceName: "profile"), selected: #imageLiteral(resourceName: "profile_selected"))
        
        viewControllers = [tagsCategoriesVC, usersTagsVC]
        
        tabBar.barTintColor = .white
        tabBar.tintColor = UIColor.foregroundColor
        tabBar.unselectedItemTintColor = UIColor(red:0.89, green:0.71, blue:0.65, alpha:1.0)
        
        guard let items = tabBar.items else {
            return
        }
        
        for item in items {
            item.imageInsets = UIEdgeInsetsMake(4, 0, -4, 0)
        }
        
//        let delimiterView: UIView = {
//            let view = UIView()
//            view.translatesAutoresizingMaskIntoConstraints = false
//            view.backgroundColor = UIColor.customPink
//            return view
//        }()
//        view.addSubview(delimiterView)
//        let safeArea = view.safeAreaLayoutGuide
//        NSLayoutConstraint.activate([
//
//            delimiterView.heightAnchor.constraint(equalToConstant: 2),
//            delimiterView.widthAnchor.constraint(equalTo: safeArea.widthAnchor),
//            delimiterView.bottomAnchor.constraint(equalTo: tabBar.topAnchor)
//
//            ])
        
        let adMobView: GADBannerView = {
            let view = GADBannerView()
            view.backgroundColor = .white
            view.translatesAutoresizingMaskIntoConstraints = false
            view.adUnitID = Constants.adMobUnitIDTest
//            view.adUnitID = Constants.adMobUnitID
            view.rootViewController = self
            let request = GADRequest()
            request.testDevices = ["E8ABFBBD-2827-4395-90B5-B2E288C91586"]
            view.load(request)
            return view
        }()
        view.addSubview(adMobView)
        let safeArea = view.safeAreaLayoutGuide
        NSLayoutConstraint.activate([

            adMobView.heightAnchor.constraint(equalToConstant: 50),
            adMobView.widthAnchor.constraint(equalToConstant: 320),
            adMobView.centerXAnchor.constraint(equalTo: safeArea.centerXAnchor),
            adMobView.bottomAnchor.constraint(equalTo: tabBar.topAnchor)
            
            ])
        
    }
    
}

// MARK: - Extension
extension UITabBarController {
    
    func generateNavigationController(for vc: UIViewController, normal normalImage: UIImage, selected selectedImage: UIImage) -> UINavigationController {
        
        let navigationController = UINavigationController(rootViewController: vc)
        vc.tabBarItem.image = normalImage
        vc.tabBarItem.selectedImage = selectedImage
        return navigationController
        
    }
    
}
