//
//  BaseVC.swift
//  Tagz
//
//  Created by Jeyhun Danyalov on 6/30/18.
//  Copyright © 2018 Jeyhun Danyalov. All rights reserved.
//

import UIKit
import Firebase

class BaseVC: UIViewController {
    
    var collectionView: UICollectionView!
    var activityIndicator: CustomActivityIndicator!
    let db = Firestore.firestore()
    
    var navigationTitle: String = "" {
        didSet {
            self.customNavController(title: navigationTitle)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customNavController(title: navigationTitle)
        
    }
    
    private func customNavController(title: String) {
        navigationItem.title = title
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationController?.navigationBar.barTintColor = .white
        navigationController?.navigationBar.tintColor = UIColor.customPink
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
        
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.customPink, NSAttributedStringKey.font: UIFont(name: "Pacifico", size: 20)!]
        navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.customPink, NSAttributedStringKey.font: UIFont(name: "Pacifico", size: 30)!]
        
//        let delimiterView: UIView = {
//            let view = UIView()
//            view.translatesAutoresizingMaskIntoConstraints = false
//            view.backgroundColor = UIColor.customPink
//            return view
//        }()
//        view.addSubview(delimiterView)
//        let safeArea = view.safeAreaLayoutGuide
//        NSLayoutConstraint.activate([
//
//            delimiterView.heightAnchor.constraint(equalToConstant: 2),
//            delimiterView.widthAnchor.constraint(equalTo: safeArea.widthAnchor),
//            delimiterView.topAnchor.constraint(equalTo: safeArea.topAnchor)
//
//            ])
    }
    
}


