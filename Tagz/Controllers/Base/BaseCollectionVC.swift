//
//  BaseCollectionVC.swift
//  Tagz
//
//  Created by Jeyhun Danyalov on 7/1/18.
//  Copyright © 2018 Jeyhun Danyalov. All rights reserved.
//

import UIKit

protocol BaseVCProtocol {
    associatedtype T:UICollectionViewCell
    var collectionViewCell: T.Type? { get }
    
    func setupCollectionView(withCell cell: T.Type)
}

class BaseCollectionVC: BaseVC, BaseVCProtocol {
    
    typealias T = UICollectionViewCell
    var collectionViewCell: T.Type?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
}

extension BaseCollectionVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {}
    
}

extension BaseCollectionVC {
    
    fileprivate func setup() {
        view.backgroundColor = .white
        setupCollectionView(withCell: collectionViewCell.self!)
        activityIndicator = CustomActivityIndicator(style: .whiteLarge, view: self.view)
    }
    
    func setupCollectionView(withCell cell: T.Type) {
        collectionView = UICollectionView(frame: CGRect(x: 0, y: 0, width: 0, height: 0),
                                          collectionViewLayout: UICollectionViewFlowLayout())
        collectionView.backgroundColor = .white
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.register(cell, forCellWithReuseIdentifier: String(describing: cell))
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        
        //
        // need for UI testing
        //
        collectionView.setValue("tagsCollectionView", forKey: "accessibilityLabel")
        view.addSubview(collectionView)
        
        
        //
        // set constraints
        //
        let safeArea = view.safeAreaLayoutGuide
        
        collectionView.widthAnchor.constraint(equalTo: safeArea.widthAnchor).isActive = true
        collectionView.topAnchor.constraint(equalTo: safeArea.topAnchor).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: safeArea.bottomAnchor, constant: -50).isActive = true
        collectionView.centerXAnchor.constraint(equalTo: safeArea.centerXAnchor).isActive = true
    }
    
}
