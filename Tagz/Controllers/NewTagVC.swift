//
//  NewTagVC.swift
//  Tagz
//
//  Created by Jeyhun Danyalov on 5/7/18.
//  Copyright © 2018 Jeyhun Danyalov. All rights reserved.
//

import UIKit

class NewTagVC: BaseVC {
    
    var titleTxtField: UITextField!
    var tagsTxtView: UITextView!
    
    override func viewDidLoad() {
        navigationTitle = "add new tag."
        super.viewDidLoad()
        
        setup()
    }
    
    @objc fileprivate func saveTags() {
        
        guard !titleTxtField.text!.isEmpty else {
            errorAlert(title: "Error", message: "Title can't be empty!")
            return
        }
        guard !tagsTxtView.text!.isEmpty else {
            errorAlert(title: "Error", message: "You must write at least one tag!")
            return
        }
        
        let tags = tagsTxtView.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        let splittedTags = tags.components(separatedBy: " ")
        let filteredArr = splittedTags.filter { tag -> Bool in
            return !tag.starts(with: "#")
        }
        guard splittedTags.count <= 30 else {
            errorAlert(title: "Error", message: "Tags max count: 30!")
            return
        }
        guard filteredArr.isEmpty else {
            errorAlert(title: "Error", message: "Every tag must start with '#' symbol!")
            return
        }
        
        let context = CustomContext.shared.getContext
        let tagModel = TagModel(context: context)
        do {
            tagModel.title = titleTxtField.text!
            tagModel.tags = tagsTxtView.text!
            
            try context.save()
        } catch {
            errorAlert(title: "Error", message: error.localizedDescription)
        }
        
        closeView()
    }
    
    @objc fileprivate func closeView() {
        dismiss(animated: true)
    }
    
}

//MARK: - Extension
extension NewTagVC {
    
    fileprivate func setup() {
        view.backgroundColor = .white
        
//        self.customNavigationController(title: "add new tag.")
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(closeView))
        
        createView()
    }
    
    private func createView() {
        
        let titleLbl: UILabel = {
            let label = UILabel()
            label.font = UIFont.systemFont(ofSize: 12)
            label.textColor = UIColor.lightGray
            label.text = "Title"
            return label
        }()
        titleTxtField = UITextField()
        titleTxtField.translatesAutoresizingMaskIntoConstraints = false
        titleTxtField.placeholder = "Title"
        titleTxtField.clearButtonMode = .whileEditing
        titleTxtField.borderStyle = .roundedRect
        titleTxtField.font = UIFont.systemFont(ofSize: 15, weight: .medium)
        titleTxtField.textColor = UIColor.customPink
        
        let titleStackView = UIStackView(arrangedSubviews: [titleLbl, titleTxtField])
        titleStackView.axis = .vertical
        titleStackView.alignment = .fill
        titleStackView.distribution = .fill
        titleStackView.spacing = 5
        
        let tagsLbl: UILabel = {
            let label = UILabel()
            label.font = UIFont.systemFont(ofSize: 12)
            label.textColor = UIColor.lightGray
            label.text = "Tags"
            return label
        }()
        tagsTxtView = UITextView()
        tagsTxtView.translatesAutoresizingMaskIntoConstraints = false
        tagsTxtView.font = UIFont.systemFont(ofSize: 15, weight: .medium)
        tagsTxtView.textColor = UIColor.customPink
        tagsTxtView.backgroundColor = UIColor.lightGray.withAlphaComponent(0.1)
        tagsTxtView.autocapitalizationType = .none
        tagsTxtView.layer.cornerRadius = 5
        //
        // for UI testting
        //
        tagsTxtView.setValue("tagsTextView", forKey: "accessibilityLabel")
        
        let tagsStackView = UIStackView(arrangedSubviews: [tagsLbl, tagsTxtView])
        tagsStackView.axis = .vertical
        tagsStackView.alignment = .fill
        tagsStackView.distribution = .fill
        tagsStackView.spacing = 5
        
        let submitBtn: UIButton = {
            let button = UIButton(type: .custom)
            button.translatesAutoresizingMaskIntoConstraints = false
            button.setTitle("SAVE", for: .normal)
            button.titleLabel?.font = UIFont.systemFont(ofSize: 17, weight: .bold)
            button.titleLabel?.textColor = UIColor.customPink
            button.backgroundColor = UIColor.customRed
            button.layer.cornerRadius = 5
            button.addTarget(self, action: #selector(saveTags), for: .touchUpInside)
            return button
        }()
        view.addSubview(submitBtn)
        
        
        let stack = UIStackView(arrangedSubviews: [titleStackView, tagsStackView])
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.axis = .vertical
        stack.alignment = .fill
        stack.distribution = .fillProportionally
        stack.spacing = 15
        view.addSubview(stack)
        
        NSLayoutConstraint.activate([
            
            stack.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.8),
            stack.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 50),
            stack.centerXAnchor.constraint(equalTo: view.centerXAnchor),

            titleTxtField.heightAnchor.constraint(equalToConstant: 40),
            tagsTxtView.heightAnchor.constraint(equalToConstant: 200),
            
            submitBtn.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            submitBtn.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -30),
            submitBtn.widthAnchor.constraint(equalTo: stack.widthAnchor),
            submitBtn.heightAnchor.constraint(equalToConstant: 50),
            
            ])
        
    }
    
}
