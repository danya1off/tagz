//
//  TagsCollectionsVC.swift
//  Tagz
//
//  Created by Jeyhun Danyalov on 5/9/18.
//  Copyright © 2018 Jeyhun Danyalov. All rights reserved.
//

import UIKit
import Firebase

class TagsVC: BaseCollectionVC {

    var tagCollection: TagCollection?
    var tagsCategories = [TagCategory]()
    
    override func viewDidLoad() {
        
        navigationTitle = tagCollection!.title.lowercased()
        navigationItem.largeTitleDisplayMode = .never
        collectionViewCell = TagsCVCell.self
        super.viewDidLoad()
        
        loadTags()
    }
    
    private func loadTags() {
        activityIndicator.startAnimating()
        guard let tag = tagCollection  else {
            activityIndicator.stopAnimating()
            errorAlert(title: "Error", message: "Tag defined incorrectly!") {
                self.navigationController?.popViewController(animated: true)
            }
            return
        }
        db.collection(Constants.tags).document(tag.id).collection(Constants.categories).getDocuments { (snapshot, error) in
            if let err = error {
                print(err.localizedDescription)
            } else {
                for doc in snapshot!.documents {
                    var tagCategory = TagCategory()
                    tagCategory.id = doc.documentID
                    if let title = doc.data()["title"] as? String {
                        tagCategory.title = title
                    }
                    if let tags = doc.data()["tags"] as? String {
                        tagCategory.tags = tags
                    }
                    self.tagsCategories.append(tagCategory)
                }
                let sortedArray = self.tagsCategories.sorted(by: {$0.title < $1.title})
                self.tagsCategories = sortedArray
            }
            DispatchQueue.main.async {
                self.activityIndicator.stopAnimating()
                self.collectionView.reloadData()
            }
        }
        
    }
    
}

extension TagsVC {
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tagsCategories.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: TagsCVCell.self), for: indexPath) as! TagsCVCell
        let tagCategory = tagsCategories[indexPath.item]
        cell.tagCategory = tagCategory
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let tagCategory = tagsCategories[indexPath.item]
        UIPasteboard.general.string = tagCategory.tags
        showToast(message: "Copied to clipboard!")
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let tagCategory = tagsCategories[indexPath.item]
        let titleString = tagCategory.title
        let tagsString = tagCategory.tags
        
        // find tags string height
        let aproximateWidthOfTitleText = view.frame.width - 52
        let titleSize = CGSize(width: aproximateWidthOfTitleText, height: 1000)
        let titleAttributes = [NSAttributedStringKey.font: Constants.tagTitleFont]
        let titleTextFrame = NSString(string: titleString).boundingRect(with: titleSize, options: .usesLineFragmentOrigin, attributes: titleAttributes, context: nil)
        
        // find tags string height
        let aproximateWidthOfTagsText = view.frame.width - 52
        let tagsSize = CGSize(width: aproximateWidthOfTagsText, height: 1000)
        let tagsAttributes = [NSAttributedStringKey.font: Constants.tagsFont]
        let tagsTextFrame = NSString(string: tagsString).boundingRect(with: tagsSize, options: .usesLineFragmentOrigin, attributes: tagsAttributes, context: nil)
        
        return CGSize(width: view.frame.width - 32, height: tagsTextFrame.height + titleTextFrame.height + 65)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 15
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 20, left: 0, bottom: 20, right: 0)
    }
}
