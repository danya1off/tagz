//
//  UsersTagsVC.swift
//  Tagz
//
//  Created by Jeyhun Danyalov on 5/7/18.
//  Copyright © 2018 Jeyhun Danyalov. All rights reserved.
//

import UIKit
import CoreData
import Fabric
import Crashlytics

class UsersTagsVC: BaseVC {
    
    var tableView = UITableView()
    
    lazy var fetchedResultsController: NSFetchedResultsController<TagModel> = {
        let fetchRequest: NSFetchRequest<TagModel> = TagModel.fetchRequest()
        let sortDescriptor = NSSortDescriptor(key: "title", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        let frc = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: CustomContext.shared.getContext, sectionNameKeyPath: nil, cacheName: nil)
        frc.delegate = self
        
        return frc
    }()
    
    override func viewDidLoad() {
        
        navigationTitle = "user's tagz."
        super.viewDidLoad()
        
        setup()
        fetch()
        
    }
    
    private func fetch() {
        do {
            try fetchedResultsController.performFetch()
        } catch {
            errorAlert(message: error.localizedDescription)
        }
    }
    
    @objc fileprivate func addNewTag() {
        let vc = NewTagVC()
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overCurrentContext
        self.present(UINavigationController(rootViewController: vc), animated: true)
    }
    
}

extension UsersTagsVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if let sections = fetchedResultsController.sections {
            return sections.count
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let sections = fetchedResultsController.sections {
            let sectionInfo = sections[section]
            return sectionInfo.numberOfObjects
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: TagsTVCell.self), for: indexPath) as! TagsTVCell
        let object = fetchedResultsController.object(at: indexPath)
        cell.tagCategory = object.getTag
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let object = fetchedResultsController.object(at: indexPath)
        let tagCategory = object.getTag
        let titleString = tagCategory.title
        let tagsString = tagCategory.tags
        
        // find tags string height
        let aproximateWidthOfTitleText = view.frame.width - 52
        let titleSize = CGSize(width: aproximateWidthOfTitleText, height: 1000)
        let titleAttributes = [NSAttributedStringKey.font: Constants.tagTitleFont]
        let titleTextFrame = NSString(string: titleString).boundingRect(with: titleSize, options: .usesLineFragmentOrigin, attributes: titleAttributes, context: nil)
        
        // find tags string height
        let aproximateWidthOfTagsText = view.frame.width - 52
        let tagsSize = CGSize(width: aproximateWidthOfTagsText, height: 1000)
        let tagsAttributes = [NSAttributedStringKey.font: Constants.tagsFont]
        let tagsTextFrame = NSString(string: tagsString).boundingRect(with: tagsSize, options: .usesLineFragmentOrigin, attributes: tagsAttributes, context: nil)
        return tagsTextFrame.height + titleTextFrame.height + 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let object = fetchedResultsController.object(at: indexPath)
        let tagCategory = object.getTag
        UIPasteboard.general.string = tagCategory.tags
        showToast(message: "Copied to clipboard!")
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        switch editingStyle {
        case .delete:
            print("delete")
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let delete = UITableViewRowAction(style: .destructive, title: "Delete") { (action, indexPath) in
            
            let alertController = UIAlertController(title: "Warning", message: "Are you sure to delete this tags?", preferredStyle: .actionSheet)
            let deleteAction = UIAlertAction(title: "Delete", style: .destructive, handler: { action in
                let tag = self.fetchedResultsController.object(at: indexPath)
                CustomContext.shared.getContext.delete(tag)
                CustomContext.shared.saveContext()
            })
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            alertController.addAction(deleteAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true)
            
        }
        delete.backgroundColor = UIColor.customRed
        
//        let share = UITableViewRowAction(style: .default, title: "Share") { (action, indexPath) in
//            print("Share")
//        }
//        share.backgroundColor = UIColor.customBlue
        return [delete]
    }
    
}


//MARK: -NSFetchedResultsControllerDelegate
extension UsersTagsVC: NSFetchedResultsControllerDelegate {
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
    
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            if let indexPath = newIndexPath {
                tableView.insertRows(at: [indexPath], with: .fade)
            }
        case .update:
            if let indexPath = indexPath {
                tableView.reloadRows(at: [indexPath], with: .automatic)
            }
        case .delete:
            if let indexPath = indexPath {
                tableView.deleteRows(at: [indexPath], with: .fade)
            }
        default:
            break
        }
    }
    
}

//MARK: - Extension
extension UsersTagsVC {
    
    fileprivate func setup() {
        view.backgroundColor = .white
        
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.backgroundColor = .white
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.separatorStyle = .none
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.register(TagsTVCell.self, forCellReuseIdentifier: String(describing: TagsTVCell.self))
        tableView.setValue("usersTagsTableView", forKey: "accessibilityLabel")
        
//        self.customNavigationController(title: "user's tagz.")
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "add"), style: .plain, target: self, action: #selector(addNewTag))
        
        createView()
        
    }
    
    private func createView() {
        
        view.addSubview(tableView)
        let safeArea = view.safeAreaLayoutGuide
        
        tableView.topAnchor.constraint(equalTo: safeArea.topAnchor, constant: 8).isActive = true
        tableView.bottomAnchor.constraint(equalTo: safeArea.bottomAnchor, constant: -50).isActive = true
        tableView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor).isActive = true
        
    }
    
}
