//
//  BaseViewController.swift
//  Tagz
//
//  Created by Jeyhun Danyalov on 6/30/18.
//  Copyright © 2018 Jeyhun Danyalov. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        custom()
    }
    
    private func custom() {
        navigationItem.title = "aaa"
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationController?.navigationBar.barTintColor = .white
        navigationController?.navigationBar.tintColor = UIColor.customPink
        navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
        
        guard let pacificoFont = UIFont(name: "Pacifico", size: 20) else {
            return
        }
        
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.customPink, NSAttributedStringKey.font: pacificoFont]
        
        let delimiterView: UIView = {
            let view = UIView()
            view.translatesAutoresizingMaskIntoConstraints = false
            view.backgroundColor = UIColor.customPink
            return view
        }()
        view.addSubview(delimiterView)
        let safeArea = view.safeAreaLayoutGuide
        NSLayoutConstraint.activate([
            
            delimiterView.heightAnchor.constraint(equalToConstant: 2),
            delimiterView.widthAnchor.constraint(equalTo: safeArea.widthAnchor),
            delimiterView.topAnchor.constraint(equalTo: safeArea.topAnchor)
            
            ])
    }
    
}
