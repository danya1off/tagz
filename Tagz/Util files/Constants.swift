//
//  Constants.swift
//  Tagz
//
//  Created by Jeyhun Danyalov on 5/7/18.
//  Copyright © 2018 Jeyhun Danyalov. All rights reserved.
//

import Foundation
import UIKit

struct Constants {
    
    private init() {}
    
    //MARK: - General params
    static let adMobID = "ca-app-pub-2562717320784018~2450354048"
    static let adMobUnitID = "ca-app-pub-2562717320784018/6038245263"
    static let adMobUnitIDTest = "ca-app-pub-3940256099942544/2934735716"

    
    //MARK: - DB Variables
    static let tags = "tags"
    static let categories = "categories"
    
    //MARK: - Text fonts
    static var tagTitleFont: UIFont {
        return UIFont.systemFont(ofSize: 18, weight: .bold)
    }
    
    static var tagsFont: UIFont {
        return UIFont.systemFont(ofSize: 13, weight: .medium)
    }
}
