//
//  TagzUITests.swift
//  TagzUITests
//
//  Created by Jeyhun Danyalov on 5/6/18.
//  Copyright © 2018 Jeyhun Danyalov. All rights reserved.
//

import XCTest

class TagzUITests: XCTestCase {
    
    var app: XCUIApplication?
    
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()
        app = XCUIApplication()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        app = nil
    }
    
    func testExample() {
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testClickToTagCell() {
        
        app!.collectionViews/*@START_MENU_TOKEN@*/.cells.containing(.image, identifier:"popular_icon")/*[[".cells.containing(.staticText, identifier:\"Popular\")",".cells.containing(.image, identifier:\"popular_icon\")"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.children(matching: .other).element.tap()
        app!.navigationBars["popular"].buttons["tagz."].tap()
        
    }
    
    func testClickToCollectionView() {
        
        let collectionView = app!.collectionViews["tagsCollectionView"]
        collectionView.cells.staticTexts["Popular"].tap()
        app!.navigationBars["popular"].buttons["tagz."].tap()
        
    }
    
    func testAddNewTag() {
        
        app!.tabBars.children(matching: .button).element(boundBy: 1).tap()
        app!.navigationBars["user's tagz."].buttons["add"].tap()
        let titleTextField = app!.textFields["Title"]
        titleTextField.tap()
        titleTextField.typeText("Title 2")
        
        let textView = app!.textViews["tagsTextView"]
        textView.tap()
        textView.typeText("#tags3 #tags4")
        app!.buttons["SAVE"].tap()
        
        let tableView = app!.tables["usersTagsTableView"]
        XCTAssertEqual(tableView.cells.count, 2)
        
    }
    
    func testAddNewTagAndDeleteIt() {
        
        let tagsText = "#tags5 #tags6"
        
        app!.tabBars.children(matching: .button).element(boundBy: 1).tap()
        
        app!.navigationBars["user's tagz."].buttons["add"].tap()
        
        let titleTextField = app!.textFields["Title"]
        titleTextField.tap()
        titleTextField.typeText("Title 3")
        
        let textView = app!.textViews["tagsTextView"]
        textView.tap()
        textView.typeText(tagsText)
        app!.buttons["SAVE"].tap()
        
        let tableView = app!.tables["usersTagsTableView"]
        tableView.staticTexts[tagsText].swipeLeft()
        tableView.buttons["Delete"].tap()
        app!.sheets["Warning"].buttons["Delete"].tap()
        
        XCTAssertEqual(tableView.cells.count, 2)
    }
    
}
