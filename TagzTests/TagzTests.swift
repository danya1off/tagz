//
//  TagzTests.swift
//  TagzTests
//
//  Created by Jeyhun Danyalov on 5/6/18.
//  Copyright © 2018 Jeyhun Danyalov. All rights reserved.
//

import XCTest
@testable import Tagz

class TagzTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
            print("Hello, world!")
        }
    }
    
    func testCellForRowAt() {
        let vc = TagsCategoriesVC()
        vc.viewDidLoad()
        XCTAssertNotNil(vc.view)
        let cell = vc.collectionView(vc.collectionView, cellForItemAt: IndexPath(item: 0, section: 0)) as! TagCategoryCell
        let tagItem = cell.tagItem
        XCTAssertNotNil(tagItem)
        
        XCTAssertEqual(tagItem?.title, "Popular")
    }
    
}
